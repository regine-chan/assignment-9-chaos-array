﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_9___Chaos_Array
{
    // Custom exception to handle fetching of empty indexes in the chaos array
    class ObjectUnavailableException : Exception
    {
        public ObjectUnavailableException() { }


        public ObjectUnavailableException(string message) : base(message) { }

        public ObjectUnavailableException(string message, Exception inner) : base(message, inner) { }
    }
}
