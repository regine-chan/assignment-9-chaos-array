﻿using System;

namespace Assignment_9___Chaos_Array
{
    class Program
    {
        static void Main(string[] args)
        {
            // Sets program state
            int ProgramState = 1;

            // Instantiates the ChaosArray to hold subclasses of Object
            ChaosArray<Object> chaosArray = new ChaosArray<object>();

            // Instantiates a random number generator
            Random rnd = new Random();

            // Welcome text
            Console.WriteLine("THIS is the CHAOS ARRAY program, enter a string or integer to insert into the array at own risk!");

            // Program begins
            while(ProgramState == 1)
            {
                // prompts the user to chose an operation, insert is default
                Console.WriteLine("Do you wish to insert or fetch an object? [0 - insert (default), 1 - fetch] : ");

                // Default set
                int OperationToDo = 0;

                // Tries to parse the input as an integer
                try
                {
                    OperationToDo = int.Parse(Console.ReadLine());
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }
                
                // Runs the insert operation
                if(OperationToDo == 0)
                {
                    // Prompts the user to enter a string or an integer
                    Console.WriteLine("Enter a string or an integer: ");

                    // Reads the input as a string
                    string StringInput = Console.ReadLine();
                    try
                    {
                        // Tries to parse the string to an integer
                        int IntegerInput = int.Parse(StringInput);

                        // Inputs the integer if parsing succeeds
                        chaosArray[rnd.Next(0, Globals.ARRAY_SIZE-1)] = IntegerInput;
                    }
                    // If parsing fails, then catch the exception and input the string into the array
                    catch (FormatException e)
                    {
                        // Random output
                        Console.WriteLine("This was not a number, huehue");

                        // Tries to input the string, throws exception if index is occupied by another item
                        try
                        {
                            chaosArray[rnd.Next(0, Globals.ARRAY_SIZE)] = StringInput;
                        }
                        catch (UnavailableSpaceException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                    }
                    // Exception thrown if the integer input tries to take an occupied space
                    catch (UnavailableSpaceException e)
                    {
                        Console.WriteLine(e.Message);
                    }

                }
                // Runs if user wants to fetch an object
                else if (OperationToDo == 1)
                {
                    // Prompts the user to enter a valid index
                    Console.WriteLine($"Enter an integer between 0 and {Globals.ARRAY_SIZE - 1} to fetch an object: ");

                    try
                    {
                        // Parses the index to an integer
                        int index = int.Parse(Console.ReadLine());

                        // Outputs the object if it exist
                        Console.WriteLine($"The item is {chaosArray[index]}  "); 
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    catch (ObjectUnavailableException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    catch (IndexOutOfRangeException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    

                    
                }

               
                // Prompts the user if they want to do more operations on the chaos array
                Console.WriteLine("Do you wish to continue? [0 - exit, 1 - continue]: ");
                try
                {
                    // Tries to parse the user input to the programstate
                    ProgramState = int.Parse(Console.ReadLine());
                }
                // Fails if the input is invalid and a FormatException is thrown
                catch (FormatException e)
                {
                    Console.WriteLine("Exiting program because of :     " + e.Message);
                    ProgramState = 0;
                }
                
            }
            // Exit message after program loop
            Console.WriteLine("Exiting program ....");
            // Prints the chaos array contents
            Console.WriteLine("The CHAOS ARRAY contains: \n" + chaosArray.ToString());
        }
    }
}
