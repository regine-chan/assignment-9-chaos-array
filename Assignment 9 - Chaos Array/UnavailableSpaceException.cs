﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_9___Chaos_Array
{
    // Exception class to handle occupied indexes in the chaos array
    class UnavailableSpaceException : Exception
    {
        public UnavailableSpaceException() { }
        

        public UnavailableSpaceException(string message) : base(message) { }

        public UnavailableSpaceException(string message, Exception inner) : base(message, inner) { }
    }
}
