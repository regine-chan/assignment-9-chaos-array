﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Assignment_9___Chaos_Array
{
    public class ChaosArray<T>
    {
        // Instantiates an array of size declared in the Globals class
        private T[] chaosArray = new T[Globals.ARRAY_SIZE];

        // Special getter and setters methods with custom exceptions
        public T this[int i]
        {
            // Throws an exception if there is no object the the inputed index
            get => chaosArray[i] == null ? throw new ObjectUnavailableException($"No object present at the index {i} ... ") : chaosArray[i];
            // Throws an exception if there is an object present at the random index
            set => chaosArray[i] = chaosArray[i] != null ? throw new UnavailableSpaceException($"Couldn't insert object, object already exist on index {i}") : value;
        }

        // Overrides the default ToString() method to print out the array contents in a nice format
        public override string ToString()
        {
            string returnString = "Chaos array items are: ";
            for (int i = 0; i < Globals.ARRAY_SIZE - 1; i++)
            {
                returnString += chaosArray[i] != null ? $"\n {i}: {chaosArray[i]}" : "";
            }
            return returnString;
        }
    }
}
