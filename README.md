CHAOS ARRAY program

This program lets you input a string or integer into a random spot in a fixed sized array.

If the spot is occupied, an exception is thrown and a message is printed.

You can also retrieve an object from the array by guessing it's index.

If the spot is empty, an exception is thrown and a message is printed.

At the end of the program, the content of the chaos array is printed.

To run this, open Visual Studio and run the application